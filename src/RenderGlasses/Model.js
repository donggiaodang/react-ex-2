import React, { Component } from "react";
import model from "./glassesImage/model.jpg";
import styles from "./image.module.css";
export default class Model extends Component {
  renderIfData() {
    if (this.props.detail.name) {
      return (
        <div className={`position-absolute ${styles.content}`}>
          <h3 className=" ps-3">{this.props.detail.name}</h3>
          <h5 className="text-left ps-3 text-light">
            {this.props.detail.desc}
          </h5>
        </div>
      );
    }
  }
  render() {
    return (
      <div className="pb-5 position-relative">
        <img className="w-50" src={model} alt="" />
        <img
          className={`position-absolute  ${styles.image}`}
          src={this.props.detail.url}
          alt=""
        />
        {this.renderIfData()}
        {/* <div className={`position-absolute ${styles.content}` }>
        <h3 className=' ps-3'>{this.props.detail.name}</h3>
        <h5 className='text-left ps-3 text-light'>{this.props.detail.desc}</h5>
      </div> */}
      </div>
    );
  }
}
